Source: calligra
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Pino Toscano <pino@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.19.0),
               gettext,
               libboost-dev,
               libboost-system-dev,
               libeigen3-dev,
               libetonyek-dev,
               libfontconfig-dev,
               libfreetype-dev,
               libgit2-dev,
               libglib2.0-dev,
               libgsl-dev,
               libkchart-dev (>= 2.7.0~),
               libkf5activities-dev (>= 5.7.0),
               libkf5archive-dev (>= 5.7.0),
               libkf5akonadi-dev,
               libkf5attica-dev,
               libkf5calendarcore-dev,
               libkf5codecs-dev (>= 5.7.0),
               libkf5completion-dev (>= 5.7.0),
               libkf5config-dev (>= 5.7.0),
               libkf5configwidgets-dev (>= 5.7.0),
               libkf5contacts-dev,
               libkf5coreaddons-dev (>= 5.7.0),
               libkf5dbusaddons-dev (>= 5.7.0),
               libkf5doctools-dev (>= 5.103.0-2~),
               libkf5guiaddons-dev (>= 5.7.0),
               libkf5i18n-dev (>= 5.7.0),
               libkf5iconthemes-dev (>= 5.7.0),
               libkf5itemviews-dev (>= 5.7.0),
               libkf5jobwidgets-dev (>= 5.7.0),
               libkf5kcmutils-dev (>= 5.7.0),
               libkf5kdelibs4support-dev (>= 5.7.0),
               libkf5khtml-dev (>= 5.7.0),
               libkf5kio-dev (>= 5.7.0),
               libkf5notifications-dev (>= 5.7.0),
               libkf5notifyconfig-dev (>= 5.7.0),
               libkf5parts-dev (>= 5.7.0),
               libkf5sonnet-dev (>= 5.7.0),
               libkf5textwidgets-dev (>= 5.7.0),
               libkf5wallet-dev (>= 5.7.0),
               libkf5widgetsaddons-dev (>= 5.7.0),
               libkf5windowsystem-dev (>= 5.7.0),
               libkf5xmlgui-dev (>= 5.7.0),
               libkproperty3-dev (>= 3.1.0~),
               libkreport3-dev (>= 3.1.0~),
               liblcms2-dev (>= 2.4),
               libodfgen-dev,
               libopenexr-dev,
               libphonon4qt5-dev,
               libphonon4qt5experimental-dev,
               libpoppler-private-dev (>= 0.83.0~),
               libpoppler-qt5-dev (>= 0.83.0~),
               libqca-qt5-2-dev,
               libqt5opengl5-dev (>= 5.3.0),
               libqt5svg5-dev (>= 5.3.0),
               libqt5x11extras5-dev (>= 5.3.0),
               libspnav-dev,
               libvisio-dev (>= 0.1),
               libwpd-dev (>= 0.10),
               libwpg-dev (>= 0.3),
               libwps-dev (>= 0.4),
               libx11-dev,
               libxml2-dev,
               okular-dev (>= 4:17.08),
               pkgconf,
               pkg-kde-tools (>= 0.15.16),
               pstoedit,
               qtbase5-dev (>= 5.3.0),
               qtdeclarative5-dev (>= 5.3.0),
               shared-mime-info,
               zlib1g-dev
Standards-Version: 4.7.0
Homepage: https://www.calligra.org/
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/calligra.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/calligra
Rules-Requires-Root: no

Package: calligra
Architecture: all
Depends: calligrasheets (>= ${source:Version}),
         calligrastage (>= ${source:Version}),
         calligrawords (>= ${source:Version}),
         karbon (>= ${source:Version}),
         ${misc:Depends}
Description: extensive productivity and creative suite
 Calligra Suite is a set of applications written to help you to accomplish
 your work. It includes office applications such as a word processor,
 a spreadsheet, a presentation program, a database application, etc., and
 raster and vector graphics tools.
 .
 This metapackage provides all the components of the Calligra Suite.

Package: karbon
Architecture: any
Section: graphics
Depends: calligra-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: pstoedit
Homepage: https://www.calligra.org/karbon/
Description: vector graphics application for the Calligra Suite
 Karbon is a vector drawing application with an user interface that is easy to
 use, highly customizable and extensible. That makes Karbon a great application
 for users starting to explore the world of vector graphics as well as for
 artists wanting to create breathtaking vector art. Features include:
 .
  * Loading support for ODG, SVG, WMF, WPG, EPS/PS
  * Writing support for ODG, SVG, WMF, PNG, PDF
  * Customizable user interface with freely placeable toolbars and dockers
  * Layer docker for easy handling of complex documents including preview
    thumbnails, support for grouping shapes via drag and drop,
    controlling visibility of shapes or locking
  * Advanced path editing tool with great on-canvas editing capabilities
  * Various drawing tools for creating path shapes including a draw path
    tool and a pencil tool, as well as a calligraphy drawing tool
  * Gradient and pattern tools for easy on-canvas editing of gradient and
    pattern styles
  * Top notch snapping facilities for guided drawing and editing (e.g.
    snapping to grid, guide lines, path nodes, bounding boxes, orthogonal
    positions, intersections of path shapes or extensions of lines and
    paths)
  * Many predefined basic shapes included, such as circle/ellipse, star or
    rectangle
  * Artistic text shape with support for following path outlines
    (i.e. text on path)
  * Complex path operations and effects like Boolean set operations,
    path flattening, rounding and refining as well as whirl/pinch effects
  * Extensible by writing plugins for new tools, shapes and dockers
 .
 This package is part of the Calligra Suite.

Package: calligrasheets
Architecture: any
Section: math
Depends: calligra-libs (= ${binary:Version}),
         calligrasheets-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: khelpcenter, python, ruby
Homepage: https://www.calligra.org/sheets/
Description: spreadsheet for the Calligra Suite
 Tables is a powerful spreadsheet application.  It is scriptable and
 provides both table-oriented sheets and support for complex mathematical
 formulae and statistics. It is the successor of KSpread.
 .
 This package is part of the Calligra Suite.

Package: calligrasheets-data
Architecture: all
Section: math
Depends: ${misc:Depends}
Homepage: https://www.calligra.org/sheets/
Description: data files for Sheets spreadsheet
 This package contains architecture-independent data files for Sheets,
 the spreadsheet shipped with the Calligra Suite.
 .
 See the calligrasheets package for further information.
 .
 This package is part of the Calligra Suite.

Package: calligrawords
Architecture: any
Section: text
Depends: calligra-libs (= ${binary:Version}),
         calligrawords-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Homepage: https://www.calligra.org/words/
Description: word processor for the Calligra Suite
 Words is a FrameMaker-like word processing and desktop publishing
 application. It is capable of creating polished and professional
 looking documents. It can be used for desktop publishing, but also for
 "normal" word processing, like writing letters, reports and so on.
 .
 This package is part of the Calligra Suite.

Package: calligrawords-data
Architecture: all
Section: text
Depends: ${misc:Depends}
Homepage: https://www.calligra.org/words/
Description: data files for Words word processor
 This package contains architecture-independent data files for Words,
 the word processor shipped with the Calligra Suite.
 .
 See the calligrawords package for further information.
 .
 This package is part of the Calligra Suite.

Package: calligrastage
Architecture: any
Depends: calligra-libs (= ${binary:Version}),
         calligrastage-data (>=${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: khelpcenter
Homepage: https://www.calligra.org/stage/
Description: presentation program for the Calligra Suite
 Stage is an easy to use yet still flexible presentation application. You can
 easily create presentations containing a rich variety of elements, from
 graphics to text, from charts to images. Stage is extensible through a
 plugin system, so it is easy to add new effects, new content elements or even
 new ways of managing your presentation.
 .
 Stage natively uses the OpenDocument file format standard, for easy
 interchange with all ODF supporting applications, which includes Microsoft
 Office. Examples of its easy-to-use features are support for layouts, a
 special slide overview view during presentations for the presenter, support
 for many different master sliders in one presentation, cool transitions and
 a useful notes feature.
 .
 This package is part of the Calligra Suite.

Package: calligrastage-data
Architecture: all
Depends: ${misc:Depends}
Homepage: https://www.calligra.org/stage/
Description: data files for Calligra Stage
 This package contains architecture-independent data files for Stage,
 the presentation program shipped with the Calligra Suite.
 .
 See the calligrastage package for further information.
 .
 This package is part of the Calligra Suite.

Package: calligra-libs
Section: libs
Architecture: any
Depends: calligra-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: fonts-lyx, libqca-qt5-2-plugins
Suggests: texlive, wordnet
Description: common libraries and binaries for the Calligra Suite
 This package provides the libraries and binaries that are shared amongst
 the various components of Calligra.
 .
 This package is part of the Calligra Suite.

Package: calligra-data
Section: libs
Architecture: all
Depends: ${misc:Depends}
Suggests: khelpcenter
Description: common shared data for the Calligra Suite
 This package provides the architecture-independent data that is shared
 amongst the various components of Calligra.
 .
 This package is part of the Calligra Suite.

Package: okular-backend-odp
Architecture: any
Section: graphics
Depends: calligra-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: okular
Enhances: okular
Description: Okular backend for ODP documents
 This package provides a backend for Okular to view OpenDocument Presentation
 (ODP) documents, but also PowerPoint and PPTX documents.
 .
 This package is part of the Calligra Suite.

Package: okular-backend-odt
Architecture: any
Section: graphics
Depends: calligra-libs (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: okular
Enhances: okular
Description: Okular backend for ODT documents
 This package provides a backend for Okular to view OpenDocument Text
 (ODT) documents, but also DOC, DOCX, RTF and WPD documents.
 .
 This package is part of the Calligra Suite.

Package: calligra-gemini
Architecture: any
Depends: calligra-libs (= ${binary:Version}),
         calligra-gemini-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Homepage: https://www.calligra.org/gemini/
Description: unified interface for stage and words
 Calligra Gemini provide a unified applications which combines
 traditional desktop application and touch friendly interface for
 the Words and Stage components.
 .
 This package is part of the Calligra Suite.

Package: calligra-gemini-data
Architecture: all
Depends: ${misc:Depends}, texlive-fonts-extra
Homepage: https://www.calligra.org/gemini/
Description: Calligra Gemini - data files
 Calligra Gemini provide a unified applications which combines
 traditional desktop application and touch friendly interface for
 the Words and Stage components.
 .
 This package provides data files for Calligra Gemini.
